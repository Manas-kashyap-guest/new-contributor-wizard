# New Contributor Wizard - Developer Documentation

### How To Build

Step 1: Install [Pipenv](https://docs.pipenv.org/)

`$ pip install pipenv`

Step 2: Clone this repository to your local machine

`$ git clone https://salsa.debian.org/new-contributor-wizard-team/new-contributor-wizard`

Step 3: Change location to repository

`$ cd new-contributor-wizard`

Step 4: Install dependencies for KIVY

- For Linux

```
# Install necessary system packages
sudo apt-get install -y \
    python-pip \
    build-essential \
    git \
    python \
    python-dev \
    ffmpeg \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev

# Install gstreamer for audio, video (optional)
sudo apt-get install -y \
    libgstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good
```

- For MacOS and Windows

This project is build and tested on Linux OS (specifically Debian 9), if you would like to deploy the application to any other OS do checkout the installation process from KIVY docs for [MacOS](https://kivy.org/docs/installation/installation-osx.html) and [Windows](https://kivy.org/docs/installation/installation-windows.html).

Step 4: Install application dependencies (this might take a while, grab a cup of coffee)

`$ pipenv install`

Step 5: Install Kivy

`$ pipenv install --skip-lock git+http://github.com/kivy/kivy.git#egg=kivy-1.10.1`

Step 6: Run New Contributor Wizard

`$ pipenv run python main.py`

### How To tests

There are two type of testing being done for this application.

- Testing for PEP8 using `pylint`

`$ pipenv run pylint main.py`

`$ pipenv run pylint modules/`

- Testing for unit and integration tests using `pytest`

`$ pipenv run pytest tests/`

- User also has option to check code coverage using `pytest-cov` plugin with `pytest`

`$ pipenv run pytest --cov=modules`

### Some Important Files/Locations

- `main.py` - It contains the Root Kivy Application which is to run in order to start the GUI.

- `settings.py` - This modules sets up the database which is later integrated with the entire application.

- `data` - It contains static application data which can be used by application at anytime. It helps when the machine is offine.

- `docs` - This directory should and only contain the documentations for developers, contributors and end users.

- `modules` - This directory should and only contain the application login for different modules integrated to the Root Application. For example, this should contain the source for all application logic the tutorials and tools.

- `tests` - This directory should and only contain the Test written for the application, both for application logic and GUI.

- `ui` - This directory should and only contain the `.kv` files which uses Kivy Language in order to create the widget tree.

### Sign Up Module

- `modules/signup/` contains the Python logic for Sign Up. They can be described as below.

    - `exceptions.py` contains SignUp module specific custom exception classes
    - `services.py` contains service functions for SignUp module
    - `signup.py` contains KIVY UI and other integrations for SignUp module
    - `validations.py` contains user information validation functions
    - `utils.py` contains utility functions to help out with SignUp operations

- `ui/signup.kv` file contains KIVY widget tree which in turn renders the UI for the Sign Up module.

- `tests/signup/` contains written tests for the Sign Up module. They can be described as below.

    - `test_services.py` - contains tests for `modules/signup/services.py`
    - `test_utils.py` - contains tests for `modules/signup/utils.py`

### SignIn Module

`modules/signin/` contains the Python logic for Sign In. They can be described as below.

    - `exceptions.py` contains SignIn module specific custom exception classes
    - `services.py` contains service functions for SignIn module
    - `signin.py` contains KIVY UI and other integrations for SignIn module
    - `validations.py` contains user information validation functions
    - `utils.py` contains utility functions to help out with SignIn operations

- `ui/signin.kv` file contains KIVY widget tree which in turn renders the UI for the Sign Up module.

- `tests/signin/` contains written tests for the Sign Up module. They can be described as below.

    - `test_services.py` - contains tests for `modules/signin/services.py`
    - `test_utils.py` - contains tests for `modules/signin/utils.py`
    - `test_validation.py` - contains tests for `modules/signin/validations.py`

### The Dashboard

- `modules/dashboard/` contains modules for Dashboard features. They can be described as below.

    - `dashboard.py` contains Dashboard class for integration with courseware modules (blog, cli, communication, encryption, how_to_use, vcs and way_ahead), settings (application, profile and theme), and the UI

- `ui/dashboard.py` contains KIVY widget tree which in turn renders the UI for the Dashboard module.

### The Courseware

- How To Use - This module helps user getting started with New Contributor Wizard application and is placed in `how_to_use/`

- Communication - This module helps user understand importance of communication and the tools involved like IRC and mailing lists and is places in `communication/`

- CLI - This module helps user learn about Command Line Interface and is places in `cli/`

- Blog - This module helps user create and deploy blogs and is placed in `blog/`

- Version Control - This module helps user get started with managing projects using Version Control System like git and is places in `vcs/`

- Encryption - This module helps user understand the importance of encryption with the help of tools used for the same and is places in `encryption/`

- Way Ahead - This module helps user reach out to differnt communities and explore open source projects and events like Google Summer of Code and is placed in `way_ahead`

### The Settings

- The settings for the entire New Contributor Wizard in places in `settings/`. They can be described as below.

    - `application.py` module contains application specific settings like language
    - `profile.py` module contains user related settings like name, email etc
    - `theme.py` module contains application theme related settings like color

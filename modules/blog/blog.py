'''
Modules containing Blog classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/blog.kv')


class Blog(BoxLayout):
    '''
    Blog class for tutorials and tools
    '''
    pass

'''
Modules containing Version Control System classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/vcs.kv')


class VCS(BoxLayout):
    '''
    VCS class for tutorials and tools
    '''
    pass

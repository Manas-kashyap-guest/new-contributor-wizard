'''
Dashboard module includes classes to showcase Dashboard with different courseware and settings
'''
import logging

from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
try:
    from libs.garden.navigationdrawer import NavigationDrawer
except ImportError:
    logging.info('Dashboard: Install navigationdrawer from garden')

from modules.blog.blog import Blog
from modules.cli.cli import CLI
from modules.communication.communication import Communication
from modules.encryption.encryption import Encryption
from modules.how_to_use.how_to_use import HowToUse
from modules.vcs.vcs import VCS
from modules.way_ahead.way_ahead import WayAhead
from modules.settings.application import ApplicationSettings
from modules.settings.profile import ProfileSettings
from modules.settings.theme import ThemeSettings


Builder.load_file('./ui/dashboard.kv')


class Dashboard(BoxLayout, Screen):
    '''
    Dashboard class to integrate courseware and settings
    '''
    pass

'''
Modules containing Application Settings classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/application_settings.kv')


class ApplicationSettings(BoxLayout):
    '''
    ApplicationSettings class for settings related to New
    Contributor Wizard application
    '''
    pass

'''
Modules containing Profile Settings classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/profile_settings.kv')


class ProfileSettings(BoxLayout):
    '''
    ProfileSettings class for settings related to signed in user
    '''
    pass

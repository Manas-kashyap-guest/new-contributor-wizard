'''
Modules containing Theme Settings classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/theme_settings.kv')


class ThemeSettings(BoxLayout):
    '''
    ThemeSettings class for settings related to Theme of application
    '''
    pass

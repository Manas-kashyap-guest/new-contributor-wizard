'''
Modules containing Way Ahead classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/way_ahead.kv')


class WayAhead(BoxLayout):
    '''
    WayAhead class for tutorials and tools
    '''
    pass
    
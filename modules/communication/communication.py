'''
Modules containing Communication classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/communication.kv')


class Communication(BoxLayout):
    '''
    Communication class for tutorials and tools
    '''
    pass

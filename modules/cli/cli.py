'''
Modules containing CLI classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/cli.kv')


class CLI(BoxLayout):
    '''
    Command Line Interface class for tutorials and tools
    '''
    pass

'''
Modules containing How To Use classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/how_to_use.kv')


class HowToUse(BoxLayout):
    '''
    HowToUser class for introduction on how to user New
    Contributor Wizard
    '''
    pass

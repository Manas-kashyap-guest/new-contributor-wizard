'''
Modules containing Encryption classes
'''
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder


Builder.load_file('./ui/encryption.kv')


class Encryption(BoxLayout):
    '''
    Encryption class for tutorials and tools
    '''
    pass
